import firebase from 'firebase';

class Backend {
  uid = '';
  messageRef = null;

  constructor() {
    var config = {
      apiKey: "AIzaSyCiocCCqyGCYS1TIBHqHllSu5BWfX-_Bls",
      authDomain: "chatapp-fbaf3.firebaseapp.com",
      databaseURL: "https://chatapp-fbaf3.firebaseio.com",
      projectId: "chatapp-fbaf3",
      storageBucket: "chatapp-fbaf3.appspot.com",
    };
    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setUid(user.uid);
      } else {
        firebase.auth().signInAnonymously().catch((error) => {
          alert(error.message);
        });
      }
    });
  }

  setUid(userId) {
    this.uid = userId;
  }

  getUid() {
    return this.uid;
  }

  loadMessages(callback) {
    this.messageRef = firebase.database().ref('messages');
    this.messageRef.off();
    const onReceive = (data) => {
      const message = data.val();
      console.log(message);
      callback({
        _id: data.key,
        text: message.text,
        createdAt: new Date(message.createAt),
        user: {
          _id: message.user._id,
          name: message.user.name
        }
      });
    };

    this.messageRef.limitToLast(20).on('child_added', onReceive);
  }

  sendMessage(message) {
    for(let i = 0; i < message.length; i++) {
      this.messageRef.push({
        text: message[i].text,
        user: message[i].user,
        createAt: firebase.database.ServerValue.TIMESTAMP,
      });
    }
  }

  closeChat() {
    if (this.messageRef) {
      this.messageRef.off();
    }
  }
}

export default new Backend();
