import React from 'react'
import {Text, View, FlatList, StyleSheet} from 'react-native'

class Room extends React.Component {
  state = {
    
  }

  constructor() {
    super()
    this.state = {
      dataSource: [{key: 'a'}, {key: 'b'}]
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => 
            <Text>{item.key}</Text>
          }
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default Room;
