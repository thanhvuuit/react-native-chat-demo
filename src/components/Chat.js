import React from 'react'
import { GiftedChat } from 'react-native-gifted-chat';
import PropTypes from 'prop-types';
import * as firebase from 'firebase';
import Backend from '../Backend';

class Chat extends React.Component {

  state = {
      messages: [],
  };

  componentWillMount() {
    // this.setState({
    //   messages: [
    //     {
    //       _id: 1,
    //       text: 'Hello developer',
    //       createdAt: new Date(),
    //       user: {
    //         _id: 2,
    //         name: 'React Native',
    //         avatar: 'https://placeimg.com/140/140/any',
    //       },
    //     },
    //   ],
    // })
  }

  // onSend(messages = []) {
  //   this.setState(previousState => ({
  //     messages: GiftedChat.append(previousState.messages, messages),
  //   }))
  // }

  render () {
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={(messages) => Backend.sendMessage(messages)}
        user={{
          _id: Backend.getUid(),
          name: this.props.username
        }}
      />

      // <GiftedChat
      //   messages={this.state.messages}
      //   onSend={messages => this.onSend(messages)}
      //   user={{
      //     _id: 1,
      //   }}
      // />
    );
  }

  componentDidMount() {
    Backend.loadMessages((messages) => {
      this.setState((previousState) => {
        return {
          messages: GiftedChat.append(previousState.messages, messages),
        };
      });
    });
  }

  componentWillUnmount() {
    Backend.closeChat();
  }
}

Chat.defaultProps = {
  username: ""
};

Chat.propTypes = {
  username: PropTypes.string,
};

export default Chat;
